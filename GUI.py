import  tkinter as tk
from tkinter import ttk

class GUI :
    def __init__(self, data):
        self.data = data
        self.window = tk.Tk()
        self.window.title('window')
        self.window.geometry('800x500')

    def main(self) :
        
        def city (locals):
            city_list = []
            for item in locals :
                city_list.append(item['parameter'][0]['parameterValue']+"("+item['parameter'][1]['parameterValue']+")")
            return list(set(city_list))

        lbl_1 = tk.Label(self.window, text='Select City', bg='yellow', fg='#263238', font=('Arial', 12))
        lbl_1.grid(column=0, row=0, pady=5)
        city_list= city(self.data['records']['location'])
        self.select_city = ttk.Combobox(self.window, 
                                values=city_list,
                                state="readonly")
        self.select_city.grid(column=0, row=1, padx=10,pady=10)
        self.select_city.current(0)
        self.select_city.bind("<<ComboboxSelected>>", self.handle_city)

        lbl_2 = tk.Label(self.window, text='Select Town', bg='green', fg='#263238', font=('Arial', 12))
        lbl_2.grid(column=1, row=0, pady=5)
        self.select_town = ttk.Combobox(self.window,values=[],state="disable")
        self.select_town.grid(column=1, row=1,padx=10,pady=10)
        self.select_town.bind("<<ComboboxSelected>>", self.handle_town)
        

        lbl_3 = tk.Label(self.window, text='Select Location', bg='gray', fg='#263238', font=('Arial', 12))
        lbl_3.grid(column=2, row=0, pady=5)
        self.select_location = ttk.Combobox(self.window,values=[],state="disable")
        self.select_location.grid(column=2, row=1,padx=10,pady=10)
        self.select_location.bind("<<ComboboxSelected>>", self.handle_location)

        self.window.mainloop()
        
    
    def handle_city(self,event):
        selected = event.widget.get()
        town_list = []
        for item in self.data['records']['location'] :
            if item['parameter'][0]['parameterValue']+"("+item['parameter'][1]['parameterValue']+")" == selected :
                town_list.append(item['parameter'][2]['parameterValue']+"("+item['parameter'][3]['parameterValue']+")")

        self.select_town['values'] = list(set(town_list))
        self.select_town.current(0)
        self.select_town['state'] = "readonly"
        self.select_city_value = selected
    
    def handle_town(self,event):
        selected = event.widget.get()
        location_list = []
        for item in self.data['records']['location'] :
            if item['parameter'][2]['parameterValue']+"("+item['parameter'][3]['parameterValue']+")" == selected :
                location_list.append(item['locationName'])

        self.select_location['values'] = list(set(location_list))
        self.select_location.current(0)
        self.select_location['state'] = "readonly"
    
    def handle_location(self,event):
        selected = event.widget.get()
        station_id = ""

        for item in self.data['records']['location'] :
            if item['locationName'] == selected :
                station_id = item['stationId']
                break

        print(station_id)